﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace pontoApi.Configs {
    public class CookiesConfigValue {
        /// <summary>
        /// Abaixo segue o value e as configuração dos cookie do Login
        /// </summary>
        public const string _STATUS = "Enviar";
        public const string DesId = "";
        public const string HtmlAtu = "hrpsenha.htm";
        public const string PrxPag = "hrgeral.htm";
        public const string NavigatorInitInfo = "Chrome";
        public const string SelLang = "";
        public const string LANGUAGESELECTED = "";

        /// <summary>
        /// Abaixo segue o name e as configuração do cookie de bater ponto
        /// </summary>
        public const string PERFILATIVO = "0";
        public const string USUSUBPERMISSAO = "0";
        public const string USUSUBDESID = "0";
        public const string EMPRESAATIVA = "17";
        public const string DEFAULTMSKDATE = "DD%2FMM%2FYYYY";
        public const string DEFAULTMSKCOMPDATE = "MM%2FYYYY";
        public const string CALCULOATIVO = "1470";
        public const string CALINIAPU = "21%2F08%2F2018";
        public const string CALFIMAPU = "20%2F09%2F2018";
        public const string CALINIACE = "21%2F08%2F2018";
        public const string CALFIMACE = "20%2F09%2F2018";
        public const string USAHELPCAMPOS = "N";
        public const string NavInfo = "Chrome";

        public const string Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
        public const string AcceptEncoding = "gzip, deflate";
        public const string AcceptLanguage = "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7";
        public const string CacheControl = "max-age=0";
        public const string Connection = "keep-alive";
        public const string ContentType = "application/x-www-form-urlencoded";
        public const string Host = "hb-vw12senior02";
        public const string Origin = "http://hb-vw12senior02";
        public const string Referer = "http://hb-vw12senior02/rondaweb/";
        public const string UpgradeInsecureRequests = "1";
        public const string UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";

        public const string ACAO = "EXCAPTURA";
        public const string SIS = "HR";
        public const string STATUS = "MARCAR";

        
        
    }
}
