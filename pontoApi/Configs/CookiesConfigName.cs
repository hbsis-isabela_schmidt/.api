﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.Configs {
    public class CookiesConfigName
    {
        /// <summary>
        /// Abaixo segue o value e as configuração dos cookie do Login
        /// </summary>
        public const string _STATUS = "STATUS";
        public const string DesId = "DesId";
        public const string HtmlAtu = "HtmlAtu";
        public const string PrxPag = "PrxPag";
        public const string NavigatorInitInfo = "NavigatorInitInfo";
        public const string SelLang = "SelLang";
        public const string LANGUAGESELECTED = "LANGUAGESELECTED";
        public const string NomUsu = "NomUsu";
        public const string SenUsu = "SenUsu";


        /// <summary>
        /// Abaixo segue o name e as configuração do cookie de bater ponto
        /// </summary>
        public const string PERFILATIVO = "PERFILATIVO";
        public const string USUSUBPERMISSAO = "USUSUBPERMISSAO";
        public const string USUSUBDESID = "USUSUBDESID";
        public const string EMPRESAATIVA = "EMPRESAATIVA";
        public const string DEFAULTMSKDATE = "DEFAULTMSKDATE";
        public const string DEFAULTMSKCOMPDATE = "DEFAULTMSKCOMPDATE";
        public const string CALCULOATIVO = "CALCULOATIVO";
        public const string CALINIAPU = "CALINIAPU";
        public const string CALFIMAPU = "CALFIMAPU";
        public const string CALINIACE = "CALINIACE";
        public const string CALFIMACE = "CALFIMACE";
        public const string USAHELPCAMPOS = "USAHELPCAMPOS";
        public const string NavInfo = "NavInfo";
        public const string USER = "USER";
        public const string CONNECTION = "CONNECTION";

        public const string Accept = "Accept";
        public const string AcceptEncoding = "Accept-Encoding";
        public const string AcceptLanguage = "Accept-Languade";
        public const string CacheControl = "Cache-Control";
        public const string Connection = "Connection";
        public const string ContentType = "Content-type";
        public const string Host = "Host";
        public const string Origin = "Origin";
        public const string Referer = "Referer";
        public const string UpgradeInsecureRequests = "Upgrade-Insecure-Requests";
        public const string UserAgent = "User-Agent";


        public const string ACAO = "ACAO";
        public const string SIS = "SIS";
        public const string STATUS = "STATUS";

        
    }
}
