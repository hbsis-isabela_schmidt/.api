﻿using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using pontoApi.DataTraceObjects;
using pontoApi.Ronda;
using pontoApi.Validations;


namespace pontoApi.Controllers
{
    [Route("[controller]")]
    public class RondaController : Controller
    {
        private readonly IRondaClient _rondaClient;

        public RondaController()
        {
            _rondaClient = new RondaClient();
        }

        [HttpPost()]
        public async Task<ActionResult<LoginResponse>> Post([FromBody] LoginResponse request)
        {
            Validacao v = new Validacao();
            var user = request.Username;
            var password = request.Password;
            var token = await _rondaClient.GetToken(request.Username, request.Password);
            //if (token == "Unauthorized")
            //{
            //    return Unauthorized();
            //}
            //else
            //{
                return(new LoginResponse
                {
                    Token = token,
                    Username = user,
                    Password = v.GerarHashMD5(password)
                });
            }
        }
    }
