﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using pontoApi.DataTraceObjects;
using pontoApi.Ronda;
using ReasonPhrases = Microsoft.AspNetCore.WebUtilities.ReasonPhrases;

namespace pontoApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase {
        private readonly RegisterClient _registerClient;

        public RegisterController() {
            _registerClient = new RegisterClient();
        }
        [HttpPost()]
        public async Task<ActionResult> Post([FromBody]LoginResponse access) {
            var user = access.Username;
            var password = access.Password;
            var token = await _registerClient.RegisterPoint(access.Username, access.Password,access.Token);
            return Ok(new LoginResponse
            {
                Token = token,
                Username = user,
                Password = password
                
            });
           
        }
        
    }
}
