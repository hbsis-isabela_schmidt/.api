﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AspNetCore.Report;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using pontoApi.DataTraceObjects;

namespace pontoApi.Controllers
{
    [Route("[controller]")]
    public class JiraController : Controller {
        private JiraApiSettings JiraApiSettings { get; set; }
        private SsrsReportSettings SsrsReportSettings { get; set; }
        private JiraDatabaseSettings JiraDatabaseSettings { get; set; }

        public JiraController(IOptions<SsrsReportSettings> ssrsReportSettings,
            IOptions<JiraDatabaseSettings> jiraDatabaseSettings,
            IOptions<JiraApiSettings> jiraApiSettings) {
            SsrsReportSettings = ssrsReportSettings.Value;
            JiraDatabaseSettings = jiraDatabaseSettings.Value;
            JiraApiSettings = jiraApiSettings.Value;
        }

        [HttpPost]
        [Route("projects")]
        public IEnumerable<JiraProject> GetProjects([FromBody]User jiraUser) {
            var client = GetHttpClient(jiraUser);

            List<JiraProject> projects = new List<JiraProject>();
            var url = $"/rest/api/2/project";
            var result = client.GetAsync(url).Result;
            var content = result.Content.ReadAsStringAsync();
            var jiraResult = JsonConvert.DeserializeObject<IEnumerable<JiraProject>>(content.Result);

            projects.AddRange(jiraResult);

            return projects.OrderBy(x => x.Name);
        }

        [HttpPost]
        [Route("boards/{projectKeyOrId}")]
        public IEnumerable<JiraBoard> GetBoards([FromBody]User jiraUser, string projectKeyOrId) {
            var client = GetHttpClient(jiraUser);

            List<JiraBoard> boardList = new List<JiraBoard>();
            bool last;
            long indexPagination = 0;
            do {
                var url = $"/rest/agile/1.0/board?startAt={indexPagination}&projectKeyOrId={projectKeyOrId}";
                var result = client.GetAsync(url).Result;
                var content = result.Content.ReadAsStringAsync();
                var jiraResult = JsonConvert.DeserializeObject<JiraResult<JiraBoard>>(content.Result);
                boardList.AddRange(jiraResult.Values);
                last = jiraResult.IsLast;
                if (!last) {
                    indexPagination += 50;
                }
            } while (!last);

            return boardList.OrderBy(x => x.Name);
        }

        [HttpPost]
        [Route("sprintsByBoard/{boardId}")]
        public IEnumerable<JiraSprint> GetBoards([FromBody]User jiraUser, int boardId) {
            var client = GetHttpClient(jiraUser);

            var url = $"/rest/agile/1.0/board/{boardId}/sprint";
            var result = client.GetAsync(url).Result;
            var content = result.Content.ReadAsStringAsync();
            var jiraResult = JsonConvert.DeserializeObject<JiraResult<JiraSprint>>(content.Result);
            return jiraResult.Values;
        }

        [HttpPost]
        [Route("issuesBySprint/{sprintId}")]
        public IEnumerable<JiraIssue> GetIssues([FromBody]User jiraUser, int sprintId) {
            var client = GetHttpClient(jiraUser);

            var url = $"/rest/agile/1.0/sprint/{sprintId}/issue";
            var result = client.GetAsync(url).Result;
            var content = result.Content.ReadAsStringAsync();
            var jiraResult = JsonConvert.DeserializeObject<JiraIssuesResult<JiraIssue>>(content.Result);
            return jiraResult.Issues;
        }

        [HttpPost]
        [Route("issuesWithWorklog")]
        public IEnumerable<Worklog> GetIssues([FromBody]QueryWorklogByPeriod query) {
            SqlConnectionStringBuilder connString = new SqlConnectionStringBuilder() {
                DataSource = JiraDatabaseSettings.SqlServer,
                InitialCatalog = JiraDatabaseSettings.DatabaseName,
                UserID = JiraDatabaseSettings.SqlUser,
                Password = JiraDatabaseSettings.SqlPassword
            };

            using (var sqlCon = new SqlConnection(connString.ConnectionString)) {
                sqlCon.Open();
                var sqlCmd = sqlCon.CreateCommand();

                sqlCmd.CommandText = @"select wl.id,
                   wl.issueid,
	               wl.timeworked,
	               wl.startdate,
	               ji.issuenum,
	               ji.SUMMARY	   
            from jiradb.dbo.worklog wl
               , jiradb.dbo.jiraissue ji 
            WHERE ji.id = wl.issueid
              and wl.author=@author
              and wl.startdate between @startdate and @enddate
            order by wl.id desc";

                sqlCmd.Parameters.AddWithValue("@author", query.JiraUser.Key);
                sqlCmd.Parameters.AddWithValue("@startdate", query.StartDate);
                sqlCmd.Parameters.AddWithValue("@enddate", query.EndDate);

                var sqlReader = sqlCmd.ExecuteReader();

                var worklogs = new List<Worklog>();
                var client = GetHttpClient(query.JiraUser);

                while (sqlReader.Read()) {
                    var worklog = new Worklog();
                    worklog.Id = Convert.ToInt32(sqlReader["id"].ToString());
                    worklog.JiraCode = sqlReader["issueid"].ToString();

                    worklog.IssueNum = sqlReader["issuenum"].ToString();
                    worklog.Summary = sqlReader["SUMMARY"].ToString();

                    var issue = GetIssueById(worklog.JiraCode, client);
                    worklog.JiraIssue = issue;

                    worklog.TimeSpent = TimeSpan.FromSeconds(Convert.ToDouble(sqlReader["timeworked"].ToString()));
                    worklog.Date = DateTime.Parse(sqlReader["startdate"].ToString());

                    worklogs.Add(worklog);
                }

                return worklogs;
            }
        }

        [HttpPost]
        [Route("issueById/{issueId}")]
        public JiraIssue GetIssue([FromBody]User jiraUser, string issueId) {
            var client = GetHttpClient(jiraUser);

            return GetIssueById(issueId, client);
        }

        private static JiraIssue GetIssueById(string issueId, HttpClient client) {
            var url = $"/rest/agile/1.0/issue/{issueId}";
            var result = client.GetAsync(url).Result;
            var content = result.Content.ReadAsStringAsync();
            var jiraResult = JsonConvert.DeserializeObject<JiraIssue>(content.Result);
            return jiraResult;
        }

        [HttpPost]
        [Route("worklogByIssue/{issueId}")]
        public JiraIssueWorklog GetWorklogs([FromBody]User jiraUser, string issueId) {
            var client = GetHttpClient(jiraUser);

            return GetWorkogByIssue(issueId, client);
        }

        private static JiraIssueWorklog GetWorkogByIssue(string issueId, HttpClient client) {
            var url = $"/rest/api/2/issue/{issueId}/worklog";
            var result = client.GetAsync(url).Result;
            var content = result.Content.ReadAsStringAsync();
            var jiraResult = JsonConvert.DeserializeObject<JiraIssueWorklog>(content.Result);
            return jiraResult;
        }

        [HttpPost]
        [Route("issueByIdWithChangelog/{issueId}")]
        public JiraIssue GetChangelog([FromBody]User jiraUser, string issueId) {
            var client = GetHttpClient(jiraUser);

            var url = $"/rest/api/2/issue/{issueId}?expand=changelog";
            var result = client.GetAsync(url).Result;
            var content = result.Content.ReadAsStringAsync();
            var jiraResult = JsonConvert.DeserializeObject<JiraIssue>(content.Result);
            return jiraResult;
        }

        [HttpPost]
        [Route("myIssues")]
        public IList<JiraIssueRecordDto> GetMyIssues([FromBody]User jiraUser) {
            JiraIssueCollection jiraResult = GetChangedIssues(jiraUser);

            var issues = new List<JiraIssueRecordDto>();

            foreach (var issue in jiraResult.Issues) {
                var worklogs = GetWorkogByIssue(issue.Key, GetHttpClient(jiraUser));

                foreach (var worklog in worklogs.Worklogs) {
                    var issueDto = new JiraIssueRecordDto(issue, worklog);

                    var epic = GetIssueById(issueDto.JiraEpicKey, GetHttpClient(jiraUser));
                    if (epic != null) {
                        issueDto.JiraEpicName = epic.Fields.Summary;
                    }

                    if (!issues.Any(x => x.JiraKey == issueDto.JiraKey)) {
                        issues.Add(issueDto);
                    }
                }

            }

            return issues.OrderBy(x => x.WorklogDate).ToList();
        }

        /// <summary>
        /// Retorna os itens com alterações realizadas pelo usuário nas últimas 2 semanas.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("changedStatusByMe")]
        public IList<JiraIssueChangeRecordDto> GetChangedStatus([FromBody]User jiraUser) {
            JiraIssueCollection jiraResult = GetChangedIssues(jiraUser);

            var issues = new List<JiraIssueChangeRecordDto>();

            foreach (var issue in jiraResult.Issues) {
                foreach (var changeHistory in issue.Changelog.Histories) {
                    if (changeHistory.Author.Key == jiraUser.Key && changeHistory.Items.Any(x => x.Field == "status")) {
                        var issueChangeRecord = new JiraIssueChangeRecord() { JiraIssue = issue, JiraChangeHistory = changeHistory };

                        var worklog = GetWorkogByIssue(issue.Key, GetHttpClient(jiraUser));

                        issues.Add(new JiraIssueChangeRecordDto(issueChangeRecord));
                    }
                }
            }

            return issues.OrderBy(x => x.ChangeDate).ToList();
        }

        private JiraIssueCollection GetChangedIssues(User user) {
            var client = GetHttpClient(user);

            var url = $"/rest/api/2/search?jql=status changed by {user.Key} after -2w&expand=changelog,customfield_10715";
            var result = client.GetAsync(url).Result;
            var content = result.Content.ReadAsStringAsync();
            var jiraResult = JsonConvert.DeserializeObject<JiraIssueCollection>(content.Result);
            return jiraResult;
        }

        [HttpPost]
        [Route("recordWorklog")]
        public async Task<bool> RecordWorklog([FromBody]WorklogRecord worklogRecord) {
            try {
                var jiraUser = worklogRecord.User;
                return await RegisterWorklog(worklogRecord.Worklog, jiraUser);
            }
            catch {
                return false;
            }
        }

        private async Task<bool> RegisterWorklog(Worklog worklog, User jiraUser) {
            var worklogJson = JsonConvert.SerializeObject(new {
                comment = worklog.Comment,
                started = worklog.Date.ToString("yyyy-MM-dd") + "T15:00:00.000-0300",
                //timeSpentSeconds = (worklog.TimeSpent - DateTime.Now.Date).TotalSeconds
                timeSpentSeconds = worklog.TimeSpent.TotalSeconds
            });

            var client = GetHttpClient(jiraUser);
            var url = $"/rest/api/2/issue/{worklog.JiraCode}/worklog";
            var content = new StringContent(worklogJson, Encoding.UTF8, "application/json");
            var result = await client.PostAsync(url, content);
            return result.StatusCode == HttpStatusCode.Created;
        }

        [HttpPost]
        [Route("updateWorklog")]
        public async Task<bool> UpdateWorklog([FromBody]WorklogRecord worklogRecord) {
            try {
                var jiraUser = worklogRecord.User;
                return await UpdateWorklog(worklogRecord.Worklog, jiraUser);
            }
            catch {
                return false;
            }
        }

        private async Task<bool> UpdateWorklog(Worklog worklog, User jiraUser) {
            var worklogJson = JsonConvert.SerializeObject(new {
                comment = worklog.Comment,
                started = worklog.Date.ToString("yyyy-MM-dd") + "T15:00:00.000-0300",
                //timeSpentSeconds = (worklog.TimeSpent - DateTime.Now.Date).TotalSeconds
                timeSpentSeconds = worklog.TimeSpent.TotalSeconds
            });

            var client = GetHttpClient(jiraUser);
            var url = $"/rest/api/2/issue/{worklog.JiraCode}/worklog/{worklog.Id}";
            var content = new StringContent(worklogJson, Encoding.UTF8, "application/json");
            var result = await client.PutAsync(url, content);
            return result.StatusCode == HttpStatusCode.OK;
        }

        [HttpPost]
        [Route("deleteWorklog/{issueIdOrKey}/{worklogId}")]
        public async Task<bool> DeleteWorklog([FromBody]User jiraUser, string issueIdOrKey, string worklogId) {
            try {
                var client = GetHttpClient(jiraUser);
                var url = $"/rest/api/2/issue/{issueIdOrKey}/worklog/{worklogId}";
                var result = await client.DeleteAsync(url);
                return result.StatusCode == HttpStatusCode.NoContent;
            }
            catch {
                return false;
            }
        }

        [HttpPost]
        [Route("recordWorklogCollection")]
        public async Task<bool> RecordWorklogCollection([FromBody]WorklogCollection worklogCollection) {
            try {
                var jiraUser = worklogCollection.User;

                foreach (Worklog worklog in worklogCollection.Worklogs) {
                    bool result = await RegisterWorklog(worklog, jiraUser);

                    if (result == false)
                        return result;
                }

                return true;
            }
            catch {
                return false;
            }
        }

        [HttpPost]
        [Route("registrosPonto")]
        public IEnumerable<BatidaPonto> RegistrosPonto([FromBody]ParametrosRelatorioPonto parametrosRelatorio) {
            string relatorioPonto = EmitirRelatorioPonto(parametrosRelatorio);

            var listaBatidasPonto = new List<BatidaPonto>();

            MapearBatidasPonto(relatorioPonto, listaBatidasPonto);

            return listaBatidasPonto
                .GroupBy(x => x.DataBatida)
                .Select(x => x.First());
        }

        [HttpPost]
        [Route("registrosPontoDetalhe")]
        public IEnumerable<BatidaPontoDetalhe> RegistrosPontoDetalhado([FromBody]ParametrosRelatorioPonto parametrosRelatorio) {
            string relatorioPonto = EmitirRelatorioPonto(parametrosRelatorio);

            var listaBatidasPonto = new List<BatidaPontoDetalhe>();

            MapearBatidasPontoDetalhe(relatorioPonto, listaBatidasPonto);

            return listaBatidasPonto;
        }

        [HttpGet]
        [Route("auditoriaPonto/{username}")]
        public IEnumerable<AuditoriaPontoJira> AuditoriaPonto(string username) {
            var auditoriaPontoJira = BuscarInconsistencias(username);

            return auditoriaPontoJira;
        }

        private IEnumerable<AuditoriaPontoJira> BuscarInconsistencias(string username) {
            List<AuditoriaPontoJira> auditorias = new List<AuditoriaPontoJira>();

            using (IDbConnection jiraConn = ConectarJira()) {
                jiraConn.Open();

                IDbCommand cmd = jiraConn.CreateCommand();
                cmd.CommandText = $@"select ponto.datapu,isnull(jira.minutos,0) minutosJira, isnull(ponto.minutos,0) minutosPonto
from v_auditoria_tempos_ronda ponto
left join v_auditoria_tempos_jira jira on ponto.username_email=jira.username_email COLLATE SQL_LATIN1_GENERAL_CP1_CI_AS
	and ponto.datapu=jira.data
where ponto.username_email='{username}'
and ISNULL(ponto.minutos,0)<>ISNULL(jira.minutos,0)
order by ponto.datapu desc";

                IDataReader reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    AuditoriaPontoJira auditoria = new AuditoriaPontoJira();
                    auditoria.Data = DateTime.Parse(reader["datapu"].ToString());
                    auditoria.MinutosJira = Int32.Parse(reader["minutosJira"].ToString());
                    auditoria.MinutosPonto = Int32.Parse(reader["minutosPonto"].ToString());
                    auditorias.Add(auditoria);
                }
            }

            return auditorias;
        }

        private IDbConnection ConectarJira() {
            SqlConnectionStringBuilder connStrBuilder = new SqlConnectionStringBuilder();

            connStrBuilder.DataSource = JiraDatabaseSettings.SqlServer;
            connStrBuilder.InitialCatalog = JiraDatabaseSettings.DatabaseName;
            connStrBuilder.UserID = JiraDatabaseSettings.SqlUser;
            connStrBuilder.Password = JiraDatabaseSettings.SqlPassword;

            return new SqlConnection(connStrBuilder.ToString());
        }

        private void MapearBatidasPonto(string relatorioPonto, List<BatidaPonto> listaBatidasPonto) {
            var batidasPonto = relatorioPonto.Split(Environment.NewLine.ToCharArray());
            for (int i = 1; i < batidasPonto.Length; i++) {
                if (string.IsNullOrEmpty(batidasPonto[i]))
                    continue;

                var colunasBatidaPonto = batidasPonto[i].Split(',');

                var diaBatida = TratarData(colunasBatidaPonto[1]);
                var horarioBatida = colunasBatidaPonto[5];
                var horasNoDiaBatida = colunasBatidaPonto[3];

                BatidaPonto batida = new BatidaPonto() {
                    DataBatida = DateTime.ParseExact(diaBatida, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    TotalDia = TimeSpan.ParseExact(horasNoDiaBatida, "c", CultureInfo.InvariantCulture)
                };
                listaBatidasPonto.Add(batida);
            }
        }

        private void MapearBatidasPontoDetalhe(string relatorioPonto, List<BatidaPontoDetalhe> listaBatidasPonto) {
            var batidasPonto = relatorioPonto.Split(Environment.NewLine.ToCharArray());
            for (int i = 1; i < batidasPonto.Length; i++) {
                if (string.IsNullOrEmpty(batidasPonto[i]))
                    continue;

                var colunasBatidaPonto = batidasPonto[i].Split(',');

                var diaBatida = TratarData(colunasBatidaPonto[1]);
                var horarioBatida = colunasBatidaPonto[5];
                var horasNoDiaBatida = colunasBatidaPonto[3];

                BatidaPontoDetalhe batida = new BatidaPontoDetalhe() {
                    DataBatida = DateTime.ParseExact(diaBatida, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    HorarioBatida = TimeSpan.ParseExact(horarioBatida, "c", CultureInfo.InvariantCulture),
                    TotalDia = TimeSpan.ParseExact(horasNoDiaBatida, "c", CultureInfo.InvariantCulture)
                };
                listaBatidasPonto.Add(batida);
            }
        }

        private string EmitirRelatorioPonto(ParametrosRelatorioPonto parametrosRelatorio) {
            var settings = new ReportSettings() {
                ReportServer = SsrsReportSettings.ReportServer,
                UserAgent = parametrosRelatorio.UsuarioRedeHbsis
            };
            var request = new ReportRequest() {
                Path = SsrsReportSettings.ReportPath,
                Parameters = BuscarParametrosRelatorioPonto(parametrosRelatorio),
                RenderType = ReportRenderType.Csv,
                ExecuteType = ReportExecuteType.Export
            };

            var viewer = new ReportViewer(settings);
            var response = viewer.Execute(request);

            var csvReader = new StreamReader(new MemoryStream(response.Data.Stream));
            var relatorioPonto = csvReader.ReadToEnd();
            return relatorioPonto;
        }

        private string TratarData(string dataParaTratar) {
            var dataStringArray = dataParaTratar.Split('/');
            return $"{dataStringArray[0].PadLeft(2, '0')}/{dataStringArray[1].PadLeft(2, '0')}/{dataStringArray[2]}"; ;
        }

        private Dictionary<string, string> BuscarParametrosRelatorioPonto(ParametrosRelatorioPonto parametrosRelatorio) {
            Dictionary<string, string> reportParams = new Dictionary<string, string>();

            reportParams.Add("ini", parametrosRelatorio.DataInicio.ToString("MM/dd/yyyy"));
            reportParams.Add("fim", parametrosRelatorio.DataFim.ToString("MM/dd/yyyy"));
            reportParams.Add("NUMCAD", parametrosRelatorio.NumeroFuncionario);
            reportParams.Add("empresa", parametrosRelatorio.NumeroEmpresa);

            return reportParams;
        }

        private HttpClient GetHttpClient(User user) {
            var client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{user.Key}:{user.Password}");
            client.BaseAddress = new Uri(JiraApiSettings.Host);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
