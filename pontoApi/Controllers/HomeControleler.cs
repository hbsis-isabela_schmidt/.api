﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.Controllers
{
    [Route("/")]
    [ApiController]

    public class HomeController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(new
            {
                Version = "1.0.0"
            });
        }
    }
}
