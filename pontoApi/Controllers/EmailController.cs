﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;
using Newtonsoft.Json;
using pontoApi.DataTraceObjects;
using pontoApi.Services;
using pontoApi.Validations;

namespace pontoApi.Controllers
{
    [Route("[controller]")]
    public class EmailController : Controller
    {
        private readonly EmailService _emailService;
        private Validacao v = new Validacao();
        public EmailController()
        {
            _emailService = new EmailService();
        }

        [HttpPost]
        public IActionResult SendEmail([FromBody]Email email)
        {
            if (email == null) return BadRequest();
            var result = _emailService.SendEmail(email);
            if (!result)
            {
                email.Success = false;
                return new BadRequestObjectResult(email);
            }

            email.Success = true;
            return new OkObjectResult(email);
        }
    }
}