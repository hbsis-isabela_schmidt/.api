﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using pontoApi.DataTraceObjects;

namespace pontoApi.Ronda {
    public class JiraClient
    {
        private const string Host = "http://jira.hbsis.com.br";
        private readonly string _username;
        private readonly string _password;



        public async Task<string> GetHttpJiraClient(string user, string password, string token)
        {
            try
            {
                var byteArray = Encoding.ASCII.GetBytes($"{user}:{password}");
                var baseAddress = new Uri(Host);
                var cookiecontainer = new CookieContainer();
                using (var handler = new HttpClientHandler() { CookieContainer = cookiecontainer })
                using (var client = new HttpClient(handler)) {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                    var content = new FormUrlEncodedContent(new KeyValuePair<string, string>[]
                    {

                    });
                    using (var response = await client.PostAsync(baseAddress, content)) {
                        IEnumerable<Cookie> responseCookie = cookiecontainer.GetCookies(baseAddress).Cast<Cookie>();
                        return responseCookie.FirstOrDefault(x => x.Name.Equals("Basic")).Value;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           // var client = new HttpClient();
            
        }
        //public HttpClient GetHttpJiraClient()
        //{
        //    var client = new HttpClient();
        //    var byteArray = Encoding.ASCII.GetBytes($"{_username}:{_password}");
        //    client.BaseAddress = new Uri(Host);
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",Convert.ToBase64String(byteArray));
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    return client;
        //}

        //public IList<JiraBoard> GetBoards()
        //{
        //    var client = GetHttpJiraClient();
        //    List<JiraBoard> boardList = new List<JiraBoard>();
        //    bool last;
        //    long indexPagination = 0;
        //    do {
        //        var url = $"/rest/agile/1.0/board?startAt={indexPagination}";
        //        var result = client.GetAsync(url).Result;
        //        var content = result.Content.ReadAsStringAsync();
        //        var jiraResult = JsonConvert.DeserializeObject<JiraResult<JiraBoard>>(content.Result);
        //        boardList.AddRange(jiraResult.Values);
        //        last = jiraResult.IsLast;
        //        if (!last) {
        //            indexPagination += 50;
        //        }
        //    } while (!last);

        //    return boardList;
        //}
        //public IList<JiraSprint> GetSprints(int boarId) {
        //    var client = GetHttpJiraClient();
        //    var url = $"/rest/agile/1.0/board/{boarId}/sprint";
        //    var result = client.GetAsync(url).Result;
        //    var content = result.Content.ReadAsStringAsync();
        //    var jiraResult = JsonConvert.DeserializeObject<JiraResult<JiraSprint>>(content.Result);
        //    return jiraResult.Values;
        //}

        //public IList<JiraIssue> GetIssues(int sprintId) {
        //    var client = GetHttpJiraClient();
        //    var url = $"/rest/agile/1.0/sprint/{sprintId}/issue";
        //    var result = client.GetAsync(url).Result;
        //    var content = result.Content.ReadAsStringAsync();
        //    var jiraResult = JsonConvert.DeserializeObject<JiraIssuesResult<JiraIssue>>(content.Result);
        //    return jiraResult.Issues;
        //}

        //public JiraIssue GetIssue(int issueId) {
        //    var client = GetHttpJiraClient();
        //    var url = $"/rest/agile/1.0/issue/{issueId}";
        //    var result = client.GetAsync(url).Result;
        //    var content = result.Content.ReadAsStringAsync();
        //    var jiraResult = JsonConvert.DeserializeObject<JiraIssue>(content.Result);
        //    return jiraResult;
        //}

        //public IList<Worklog> GetWorklogs(string jiraCode) {
        //    var client = GetHttpJiraClient(); 
        //    var url = $"/rest/api/2/issue/{jiraCode}/worklog";
        //    var result = client.GetAsync(url).Result;
        //    var content = result.Content.ReadAsStringAsync();
        //    var jiraResult = JsonConvert.DeserializeObject<JiraResult<Worklog>>(content.Result);
        //    return jiraResult.Values;
        //}

    }
}
