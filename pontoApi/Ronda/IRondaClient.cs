﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.Ronda {
    public interface IRondaClient
    {
        Task<string> GetToken(string user, string password);
    }
}
