﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl.Http;
using pontoApi.Configs;
using pontoApi.DataTraceObjects;

namespace pontoApi.Ronda
{
    public class RegisterClient
    {
        public const string Url = "http://hb-vw12senior02/rondaweb/conector";

        public async Task<string> RegisterPoint(string user, string password, string token)
        {
            var randomValue = new Random().Next(50000, 10000000);
            var baseAddress = new Uri($"http://hb-vw12senior02/rondaweb/conector");
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() {CookieContainer = cookieContainer})
            using (var client = new HttpClient(handler))
            {
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.PERFILATIVO,CookiesConfigValue.PERFILATIVO));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.USUSUBPERMISSAO,CookiesConfigValue.USUSUBPERMISSAO));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.USUSUBDESID, CookiesConfigValue.USUSUBDESID));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.EMPRESAATIVA, CookiesConfigValue.EMPRESAATIVA));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.DEFAULTMSKDATE, CookiesConfigValue.DEFAULTMSKDATE));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.DEFAULTMSKCOMPDATE, CookiesConfigValue.DEFAULTMSKCOMPDATE));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.CALCULOATIVO, CookiesConfigValue.CALCULOATIVO));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.CALINIAPU, CookiesConfigValue.CALINIAPU));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.CALFIMAPU, CookiesConfigValue.CALFIMAPU));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.CALINIACE, CookiesConfigValue.CALINIACE));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.CALFIMACE, CookiesConfigValue.CALFIMACE));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.USAHELPCAMPOS, CookiesConfigValue.USAHELPCAMPOS));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.NavInfo, CookiesConfigValue.NavInfo));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.USER, user));
                cookieContainer.Add(baseAddress, new Cookie(CookiesConfigName.CONNECTION, token));

                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.Accept,CookiesConfigValue.Accept);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.AcceptEncoding, CookiesConfigValue.AcceptEncoding);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.AcceptLanguage,CookiesConfigValue.AcceptLanguage);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.CacheControl, CookiesConfigValue.CacheControl);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.Connection, CookiesConfigValue.Connection);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.ContentType,CookiesConfigValue.ContentType);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.Host, CookiesConfigValue.Host);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.Origin, CookiesConfigValue.Origin);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.Referer, CookiesConfigValue.Referer);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.UpgradeInsecureRequests, CookiesConfigValue.UpgradeInsecureRequests);
                client.DefaultRequestHeaders.TryAddWithoutValidation(CookiesConfigName.UserAgent,CookiesConfigValue.UserAgent);

                var content = new FormUrlEncodedContent(new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>(CookiesConfigName.ACAO, CookiesConfigValue.ACAO),
                    new KeyValuePair<string, string>(CookiesConfigName.SIS, CookiesConfigValue.SIS),
                    new KeyValuePair<string, string>(CookiesConfigName.STATUS, CookiesConfigValue.STATUS),

                });
                using (var response = await client.PostAsync(baseAddress, content))
                {
                    IEnumerable<Cookie> responseCookies = cookieContainer.GetCookies(baseAddress).Cast<Cookie>();
                    return responseCookies.FirstOrDefault(d => d.Name.Equals("CONNECTION")).Value;
                }
            }
        }
    }
}

