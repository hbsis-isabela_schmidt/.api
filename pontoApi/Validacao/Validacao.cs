﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MimeKit.Cryptography;
using pontoApi.Configs;
using pontoApi.Resources;

namespace pontoApi.Validations {
    public class Validacao {
        public bool EmailValidation(string AdressEmail)
        {
            try
            {
                string text_Validate = AdressEmail;
                Regex expressaoRegex = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
                if (expressaoRegex.IsMatch(text_Validate))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void PasswordValidate(string senha)
        {
            StringBuilder builder = new StringBuilder();
            if (string.IsNullOrWhiteSpace(senha))
            {
                builder.Append(Resource.SenhaNaoAutorizada);
            }

            if (builder.Length > 0)
            {
                throw  new Exception(builder.ToString());
            }
        }
        public string GerarHashMD5(string input)
        {
            MD5 md5Hash = MD5.Create();

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        private int mes = DateTime.Today.Month;
        private int dia = DateTime.Today.Day;
        private int ano = DateTime.Today.Year;
        public int HorasTrabalhadas = 8;
        private int HorasTotalTrabalhadas = 0;
        public void ValidarHorasTrabalhadas()
        {
            for (int dia = 1; dia <= DateTime.DaysInMonth(ano, mes); dia++) {
                switch (new DateTime(ano, mes, dia).DayOfWeek) {
                    case DayOfWeek.Saturday:
                    case DayOfWeek.Sunday:
                        break;
                    default:
                        HorasTotalTrabalhadas++;
                        break;
                }
            }
        }
        public static void ValidarConfiguracaoJiraya()
        {
            //HoraSaidaAlmoço - horaChegadaTrabalho = horasTrabalhadasManha

            //HoraSaidaTrabalho - HoraChegadaAlmoço = horasTrabalhadasTarde

            //HorasTrabalhadasManha + horastrabalhadasTarde = horasTotalDoDia

            //horasTotalDoDia - 8 = totalExtraDia



        }
    }
}
