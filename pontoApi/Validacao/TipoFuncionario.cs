﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.Configs {
    public enum TipoFuncionario {
        CLT = 2,
        Estagiario=4,
        MenorAprendiz=8,
        Admin=16
    }
}
