﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.DataTraceObjects
{
    public class JiraProject
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description => $"{Key} - {Name}";
    }

    public class JiraBoard
    {
        public int Id { get; set; }
        public string Self { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class JiraSprint
    {
        public int Id { get; set; }
        public string Self { get; set; }
        public string State { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int OriginBoardId { get; set; }
    }

    public class JiraIssue
    {
        public int Id { get; set; }
        public string Self { get; set; }
        public string Key { get; set; }
        public JiraIssueFields Fields { get; set; }
        public JiraChangelog Changelog { get; set; }
    }

    public class JiraChangelog
    {
        public IList<JiraChangeHistory> Histories { get; set; }
    }

    public class JiraChangeHistory
    {
        public long Id { get; set; }
        public User Author { get; set; }
        public DateTime Created { get; set; }
        public IList<JiraChangeItem> Items { get; set; }
    }

    public class JiraChangeItem
    {
        public string Field { get; set; }
        public FieldTypeEnum FieldType { get; set; }
        public object From { get; set; }
        public string FromString { get; set; }
        public object To { get; set; }
        public new string ToString { get; set; }
    }

    public enum FieldTypeEnum
    {
        Custom,
        Jira,
        Status
    }

    public class JiraIssueFields
    {
        public JiraIssueEpic Epic { get; set; }
        public string Summary { get; set; } //Descrição estória
        public string Customfield_10712 { get; set; } //Pontuação

        /// <summary>
        /// Épico
        /// </summary>
        public string Customfield_10715 { get; set; }

        public IList<JiraIssueSubTasks> Subtasks { get; set; }
        public JiraIssueWorklog Worklog { get; set; }
        public JiraIssueType Issuetype { get; set; }
        public int? Timespent { get; set; }
        public JiraProject Project { get; set; }
        public JiraStatus Status { get; set; }
    }

    public class JiraStatus
    {
        public string Name { get; set; }
    }

    public class JiraIssueType
    {
        public bool Subtask { get; set; } //é subtarefa?
    }

    public class JiraIssueEpic
    {
        public string Name { get; set; }
    }

    public class JiraIssueWorklog
    {
        public int Total { get; set; }
        public IList<JiraIssueWorklogs> Worklogs { get; set; }
    }

    public class JiraIssueWorklogs
    {
        public int? TimeSpentSeconds { get; set; }
        public string Started { get; set; }
    }

    public class JiraIssueSubTasks
    {
        public int Id { get; set; }
        public string Self { get; set; }
        public string Key { get; set; }
        public JiraIssueFields Fields { get; set; }
    }

    public class JiraIssueSubTasksFields
    {
        public string Summary { get; set; } //Descrição subtarefa
    }

    public class JiraIssuesResult<T>
    {
        public int MaxResults { get; set; }
        public int StartAt { get; set; }
        public bool IsLast { get; set; }
        public IList<T> Issues { get; set; }
    }

    public class JiraIssueCollection
    {
        public int MaxResults { get; set; }
        public int StartAt { get; set; }
        public bool IsLast { get; set; }
        public IEnumerable<JiraIssue> Issues { get; set; }
    }

    public class JiraHistoriesResult<T> : JiraResult<T>
    {
        public IList<T> Histories { get; set; }
    }

    public class JiraResult<T>
    {
        public int MaxResults { get; set; }
        public int StartAt { get; set; }
        public bool IsLast { get; set; }
        public IList<T> Values { get; set; }
    }

    public class Worklog
    {
        /// <summary>
        /// Identificador do registro de horas.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Pode ser o ID numérico, ou a Chave (JIRA-123).
        /// </summary>
        public string JiraCode { get; set; }

        public JiraIssue JiraIssue { get; set; }

        public string IssueNum { get; set; }
        public string Summary { get; set; }

        public TimeSpan TimeSpent { get; set; }
        public DateTime Date { get; set; }
        public string Comment { get; set; }
    }

    public class WorklogRecord
    {
        public Worklog Worklog { get; set; }
        public User User { get; set; }
    }

    public class User
    {
        public string Key { get; set; }
        public string Password { get; set; }
    }

    public class WorklogCollection
    {
        public IEnumerable<Worklog> Worklogs { get; set; }
        public User User { get; set; }
    }

    public class JiraIssueChangeRecord
    {
        public JiraIssue JiraIssue { get; set; }
        public JiraChangeHistory JiraChangeHistory { get; set; }
    }

    public class JiraIssueChangeRecordDto
    {
        private JiraIssueChangeRecord _changeRecord;
        private JiraIssueWorklogs _worklog;

        public JiraIssueChangeRecordDto(JiraIssueChangeRecord changeRecord)
        {
            _changeRecord = changeRecord;
        }

        public JiraIssueChangeRecordDto(JiraIssueChangeRecord changeRecord, JiraIssueWorklogs worklog)
        {
            _changeRecord = changeRecord;
            _worklog = worklog;
        }

        public long JiraChangeId => _changeRecord.JiraChangeHistory.Id;

        public string JiraKey => _changeRecord.JiraIssue.Key;

        public string JiraSummary => _changeRecord.JiraIssue.Fields.Summary;

        public int JiraTimespent => _worklog?.TimeSpentSeconds ?? 0;

        public string ChangeDate => _changeRecord.JiraChangeHistory.Created.ToString("dd/MM/yyyy HH:mm");

        public string FromString =>
            _changeRecord.JiraChangeHistory.Items.FirstOrDefault(x => x.Field == "status").FromString;

        public new string ToString =>
            _changeRecord.JiraChangeHistory.Items.FirstOrDefault(x => x.Field == "status").ToString;

        public string ChangeDescription => $"JiraKey:{JiraKey}, Date:{ChangeDate}, From:{FromString}, To: {ToString}";
    }

    public class JiraIssueRecordDto
    {
        private JiraIssue _issue;
        private JiraIssueWorklogs _worklog;

        public JiraIssueRecordDto(JiraIssue jiraIssue, JiraIssueWorklogs worklog)
        {
            _issue = jiraIssue;
            _worklog = worklog;
        }

        public string JiraEpicKey => _issue.Fields.Customfield_10715;

        public string JiraEpicName { get; set; }

        public string JiraKey => _issue.Key;

        public string JiraSummary => _issue.Fields.Summary;

        public int JiraTimespent => _worklog?.TimeSpentSeconds ?? 0;

        public string JiraStatus => _issue.Fields.Status.Name;

        public string WorklogDate => _worklog?.Started;

        public string ProjectKey => _issue.Fields.Project.Key;

        public string ProjectName => _issue.Fields.Project.Name;
    }

    public class QueryWorklogByPeriod
    {
        public User JiraUser { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}   
    



