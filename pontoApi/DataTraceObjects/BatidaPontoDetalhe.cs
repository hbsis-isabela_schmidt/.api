﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.DataTraceObjects {
    public class BatidaPontoDetalhe:BatidaPonto {
        public TimeSpan HorarioBatida { get; set; }
    }
}
