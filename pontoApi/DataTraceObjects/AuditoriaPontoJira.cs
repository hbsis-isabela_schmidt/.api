﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.DataTraceObjects {
    public class AuditoriaPontoJira {
        public DateTime Data { get; set; }
        public int MinutosJira { get; set; }
        public int MinutosPonto { get; set; }
    }
}
