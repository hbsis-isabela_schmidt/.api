﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.DataTraceObjects {
    public class BatidaPonto {
        public DateTime DataBatida { get; set; }
        public TimeSpan TotalDia { get; set; }
    }
}
