﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.DataTraceObjects
{
    public class Email
    {
        public string NameView { get; set; }
        public string User { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string Hour { get; set; }
        public string Minute { get; set; }
        public string Second { get; set; }
        public string Reason { get; set; }
        public string To { get; set; }
        public string Subject => "Alteração de ponto";
        public string Message => "<h3>Olá,</h3> " +
                                 $"<p style='font-size: 14px'>O funcionário <span style='font-weight: bold;'>{NameView}</span>, deseja modificar o ponto marcado:</p>" +
                                 $"<p style='font-weight: bold; font-size: 14px'>Data: {Day}/{Month}/{Year}</p>" +
                                 $"<p style='font-weight: bold; font-size: 14px'>Hora: {Hour}:{Minute}:{Second}</p>" +
                                 $"<p style='font-size: 14px'>Motivo: {Reason}.</p>";
        public string Mail => $"{User}@hbsis.com.br";
        public bool Success { get; set; }
    }
}
