﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace pontoApi.DataTraceObjects 
{
    public class ParametrosRelatorioPonto {
        //public NetworkCredential UsuarioRedeHbsis { get; set; }
        public string UsuarioRedeHbsis { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public string NumeroFuncionario { get; set; }
        public string NumeroEmpresa => "17";
    }
}
