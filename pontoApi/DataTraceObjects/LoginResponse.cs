﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pontoApi.DataTraceObjects 
{
    public class LoginResponse 
    {
        [Required(ErrorMessage = "Digite o login")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Digite a senha")]
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
