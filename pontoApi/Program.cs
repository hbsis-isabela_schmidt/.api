﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace pontoApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //        //var host = new WebHostBuilder()
            //        //    .UseKestrel()
            //        //    .UseContentRoot(Directory.GetCurrentDirectory())
            //        //    .UseSetting("detailedErrors", "true")
            //        //    .UseIISIntegration()
            //        //    .UseStartup<Startup>()
            //        //    .CaptureStartupErrors(true)
            //        //    .Build();
            //        //host.Run();

            //        CreateWebHostBuilder(args).Build().Run();
            //    }

            //    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            //        WebHost.CreateDefaultBuilder(args)
            //            .UseHttpSys(options => { options.UrlPrefixes.Add("http://*:8001/api/ponto-api/"); })
            //            .UseStartup<Startup>();

            //}

            var exePath = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            var directoryPath = Path.GetDirectoryName(exePath);

            var host = new WebHostBuilder()
                //.UseHttpSys(options => { options.UrlPrefixes.Add("http://*:8001/api/ponto-api/"); })
                .UseKestrel()
                .UseContentRoot(directoryPath)
                .UseStartup<Startup>()
                .UseUrls("http://*:5000/","http://localhost:5000/")
                .Build();

            if (Debugger.IsAttached || args.Contains("--debug"))
            {
                host.Run();
            }
            else
            {
                host.RunAsService();
            }
        }
    }
}