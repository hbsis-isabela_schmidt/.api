﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using Newtonsoft.Json;
using pontoApi.DataTraceObjects;
using pontoApi.Resources;
using SmtpClient = System.Net.Mail.SmtpClient;


namespace pontoApi.Services
{
    public class EmailService
    {
        public bool SendEmail(Email email)
        {
                SmtpClient client = new SmtpClient();
                client.Host = Resource.Host;
                client.Port = 587;
                client.Timeout = 10000;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(Resource.EmailPadrao, "@JirayaHbsis");

                MailMessage objeto_mail = new MailMessage();
                objeto_mail.From = new MailAddress(Resource.EmailPadrao, "Jiraya");
                objeto_mail.To.Add(new MailAddress($"{email.To}", "Coordenador"));
                objeto_mail.CC.Add(new MailAddress($"{email.Mail}", $"{email.User}"));
                objeto_mail.Subject = $"{email.Subject}";
                objeto_mail.SubjectEncoding = Encoding.UTF8;
                objeto_mail.BodyEncoding = Encoding.UTF8;
                objeto_mail.IsBodyHtml = true;
                objeto_mail.Body = $"{email.Message}";
            try
            {
                client.Send(objeto_mail);
                return true;
            }   
            
            catch(Exception e)
            {
                
                return false;
            }
        }
    }
}
