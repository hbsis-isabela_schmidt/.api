using System;
using System.Threading.Tasks;
using NSubstitute;
using pontoApi.Ronda;
using Xunit;
using FluentAssertions;

namespace Hbsis.PointJiraya {
    public class TestePoint
    {
        private readonly IRondaClient _rondaClient;

        public TestePoint()
        {
            _rondaClient = new RondaClient();
        }

        [Fact] 
        public void LoginComSenhaCorreta()
        {
           var login = _rondaClient.GetToken("matheus.santos", "Casa1521");
            login.Result.Should().NotBeEmpty();
        }

        [Fact]
        public void LoginComSenhaErrada() {
            var login = _rondaClient.GetToken("matheus.santos", "123");
            login.Result.Should().NotBeNullOrEmpty();
        }
    }
}
